
module cpu(input wire [15:0]data_in,
           input wire clk, run, mem_sw,
           inout wire w_disable,
           output reg [15:0]out,
           output wire [17:0]LEDR,
           output wire [8:0]LEDG);
// CPU instance, behavior is described here

wire [15:0]ram_out;    //RAM's output
reg [15:0]ram_data;    //Input for the RAM for values to be written
reg [15:0]ram_addr;    //RAM's address
reg [15:0]acc;        //Arithmetic accumulator, currently 16bit wide
reg [15:0]jmp_back;    //A register for adresses to jump back to when referring to a different point in memory
reg [7:0]instruction; //The instruction register
reg w_enable;         //used to store values in memory
reg [3:0]clk_tick = 0;//Clock tick register, neccessary to declare instructions that access memory
wire half_clk;        //Clock with half the speed of the clock used in this instance
single_port_ram ram_inst1(ram_data,ram_addr,w_enable,clk,ram_out); //Memory with twice the clock speed used for operation
clk_div #(2) div_instance1(clk, half_clk); //create the half speed clock
assign LEDG[3:0] = clk_tick[3:0]; // make LEDG0-3 represent the current tick position
assign LEDG[6:4] = instruction;   // make LEDG4-6 represent the 3 lower bits of the current instruction
assign LEDG[8] = instruction[7];   // make LEDG8 light up if the instruction referrs to a 16 bit instruction
assign LEDR[15:0] = (ram_out & {16{mem_sw}}) | (ram_addr & {16{~mem_sw}});
// make LEDR0-15 output the ram output or addr. use mem_sw as indicator of what is shown
assign LEDR[16] = mem_sw;
assign LEDR[17] = run;            // make LEDR17 represent the current mode of operation

always @(posedge half_clk)
//Actual behavior of the CPU, has to run at half the speed of the ram, because the ram needs two ticks
//to select an adress and output it's value to the ram_out, the same with write actions
begin
   out <= acc;  //Use the general output register as a representation of the ACC
   if(run) //execution mode
   begin
      clk_tick <= clk_tick +1;
      if(clk_tick == 3'd0) // instruction loader
      begin
         instruction <= ram_out[7:0];
      end
      
      else if(clk_tick == 4'd1)
      begin
         if(instruction == 7'h0) // NOP instruction
         begin
            ram_addr <= ram_addr+1;
            clk_tick <= 0;
         end
         
         else if (instruction==8'h1) // LOAD8 instruction
         begin
            jmp_back <= ram_addr+1;
            ram_addr[7:0]<=ram_out[15:8];
         end
         
         else if (instruction==8'h02) // JMP8 instruction
         begin
            ram_addr[7:0] <=ram_out[15:8];
            clk_tick <= 0;
         end

         else if (instruction==8'h03) // SAV8 instrucion
         begin
            jmp_back <= ram_addr+1;
            ram_addr[7:0]<=ram_out[15:8];
         end
         
         else if (instruction==8'h04) //ADD8 instruction
         begin
            jmp_back <= ram_addr+1;
            ram_addr[7:0]<=ram_out[15:8];
         end
         
         else if (instruction==8'h05) //SUB8 instruction
         begin
            jmp_back <= ram_addr+1;
            ram_addr[7:0]<=ram_out[15:8];
         end
         
         else if (instruction==8'h81) // LOAD16 instruction
         begin
            ram_addr <=ram_addr+1;
         end
         
         else if (instruction==8'h82) // JMP16 instruction
         begin
            ram_addr <=ram_addr+1;
         end
         
         else if (instruction==8'h83) // SAV16 instrucion
         begin
            ram_addr <= ram_addr+1;
         end
         
         else if (instruction==8'h84) // ADD16 instruction
         begin
            ram_addr <= ram_addr+1;
         end
         
         else if (instruction== 8'h85) // SUB16 instruction
         begin
            ram_addr <= ram_addr+1;
         end
         
      end
   
      else if(clk_tick == 4'd2)
      begin
         if (instruction==8'h1) // LOAD8 instruction
         begin
            acc<=ram_out;
            ram_addr<=jmp_back;
            clk_tick <=0;
         end
      
         else if (instruction==8'h03) // SAV8 instrucion
         begin
            ram_data <= acc;
            w_enable <= 1;
         end
         
         else if (instruction == 8'h04) //ADD8 instruction
         begin
            acc <= acc+ram_out;
            ram_addr <= jmp_back;
            clk_tick <= 0;
         end
         
         else if (instruction == 8'h05) //SUB8 instruction
         begin
            acc <= acc-ram_out;
            ram_addr <= jmp_back;
            clk_tick <= 0;
         end
         
         if (instruction==8'h81) // LOAD16 instruction
         begin
            jmp_back <= ram_addr+1;
            ram_addr<=ram_out;
         end
         
         else if (instruction==8'h82) // JMP16 instruction
         begin
            ram_addr <=ram_out;
            clk_tick <= 0;
         end
         
         else if (instruction==8'h83) // SAV16 instrucion
         begin
            jmp_back <= ram_addr+1;
            ram_addr<=ram_out;
         end
         
         else if (instruction==8'h84) //ADD16 instruction
         begin
            jmp_back <= ram_addr+1;
            ram_addr<=ram_out;
         end
         
         else if (instruction==8'h85) //SUB16 instruction
         begin
            jmp_back <= ram_addr+1;
            ram_addr<=ram_out;
         end
         
      end
      
      else if(clk_tick == 4'd3)
      begin
         if (instruction==8'h03) // SAV8 instrucion
         begin
            w_enable <= 0;
            ram_addr <= jmp_back;
            clk_tick <=0;
         end
   
         else if (instruction==8'h81) //LOAD16 instruction
         begin
            acc<=ram_out;
            ram_addr<=jmp_back;
            clk_tick <=0;
         end
         
         else if (instruction==8'h83) // SAV16 instrucion
         begin
            ram_data <= acc;
            w_enable <= 1;
         end
         
         else if (instruction == 8'h84) //ADD16 instruction
         begin
            acc <= acc+ram_out;
            ram_addr <= jmp_back;
            clk_tick <= 0;
         end
         
         else if (instruction == 8'h85) //SUB16 instruction
         begin
            acc <= acc-ram_out;
            ram_addr <= jmp_back;
            clk_tick <= 0;
         end
         
      end
   
      else if(clk_tick == 4'd4)
      begin
         if (instruction==8'h83) // SAV16 instrucion
         begin
            w_enable <= 0;
            ram_addr <= jmp_back;
            clk_tick <=0;
         end
   
      end
   
   end
   
   else // Programming mode
   begin
      clk_tick = 4'b0000;
      w_enable = ~w_disable;
      if (~mem_sw) //select address
      begin
         ram_addr = data_in[15:0];
      end
      
      else
      begin
         ram_data = data_in[15:0];
      end
      
   end

end
endmodule
