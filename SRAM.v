module single_port_ram
//RAM module as copied from the Intel homepage.
(
   input [15:0] data,
   input [15:0] addr,
   input we, clk,
   output [15:0] q
);

   // Declare the RAM variable
   reg [15:0] ram[65535:0]; // 2 byte per cell, 65535 cells
   
   // Variable to hold the registered read address
   reg [15:0] addr_reg; //16 bit address bus
   
   always @ (posedge clk)
   begin
   // Write
      if (we)
         ram[addr] <= data;

      addr_reg <= addr;
   end
      
   // Continuous assignment implies read returns NEW data.
   // This is the natural behavior of the TriMatrix memory
   // blocks in Single Port mode.  
   assign q = ram[addr_reg];
   
endmodule
