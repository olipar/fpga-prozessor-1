module debounce (input wire button, clk, output reg debounced_button);
//Used to debounce the input signal "button", a clock not faster than 20Hz is advised
reg old_state, tick;
initial debounced_button = 1'b0;
initial tick = 1'b0;
initial old_state = 1'b0;
always @(posedge clk)
begin
   if (~tick)
   begin
      old_state = button;
      tick <= 1'b1;
   end
   
   if (tick)
   begin
      if(old_state == button)
         debounced_button = button;
      tick <= 1'b0;
   end
		
end
	
endmodule
