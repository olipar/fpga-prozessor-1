/* as quartus includes these automatically I don't have to include them here...
`include "7Digit.v"
`include "debouncer.v"
`include "clock_divider.v"
`include "SRAM.v"
`include "CPU.v"
*/
module motherboard (input wire CLOCK_50, input wire [3:0]KEY, // main module
                    input wire [17:0]SW,
                    output wire [8:0]LEDG, output wire [17:0]LEDR,
                    output wire [6:0]HEX4, output wire [6:0]HEX5,
                    output wire [6:0]HEX6, output wire [6:0]HEX7,
                    output wire [6:0]HEX0, output wire [6:0]HEX1,
                    output wire [6:0]HEX2, output wire [6:0]HEX3);
parameter base_pps = 50000000; // Clock's pps
wire [7:0]ram_addr;
wire [7:0]ram_data;
wire [7:0]ram_out;
wire [15:0]cpu_out;            // Output of the CPU for the 7-Segment display
wire run,clk_4hz,clk_20hz,debounced_btn,deb_btn_counter;

clk_div #(base_pps/4) div_instance1(CLOCK_50, clk_4hz);   // create a 4 Hz clock
clk_div #(base_pps/20) div_instance2(CLOCK_50, clk_20hz); // create a 20 Hz clock
debounce debounce_inst1(KEY[0], clk_20hz,debounced_btn);
to_seven_digit hex_inst0(SW[3:0],HEX4);        //use the 7-Segment to show the value the switches represent
to_seven_digit hex_inst1(SW[7:4],HEX5);
to_seven_digit hex_inst2(SW[11:8],HEX6);
to_seven_digit hex_inst3(SW[15:12],HEX7);
to_seven_digit hex_inst4(cpu_out[3:0],HEX0);   //Assign the CPU-OUT register to 7-Segment displays
to_seven_digit hex_inst5(cpu_out[7:4],HEX1);
to_seven_digit hex_inst6(cpu_out[11:8],HEX2);
to_seven_digit hex_inst7(cpu_out[15:12],HEX3);
assign run=SW[17];                             //Let the run/program switch be SW17

cpu cpu_inst(SW[15:0], clk_20hz, run, SW[16], debounced_btn, cpu_out[15:0],LEDR,LEDG); //create a cpu

endmodule
