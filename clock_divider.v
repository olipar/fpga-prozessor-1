module clk_div #(parameter divider) (input wire src, output reg out);
//takes pulses of a given clock and sends out pulses according to the divider parameter
//(base_pps) as parameter will give a 1Hz clock as reg out
initial out = 1'b0;
reg [31:0] i=0;
always @(posedge src)
begin
   i = i+1;
   if (i == divider)
   begin
      out= ~out;
      i=0;
   end
	
end

endmodule
